import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  Inject,
  InternalServerErrorException,
  Query,
  Put,
} from '@nestjs/common';
import { ApiBody, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { CountriesService } from './countries.service';
import { Countries } from '../entity/countries.entity';

import { Response } from 'express';
import { CreateResultDTO, DeleteDTO, UpdateResultDTO } from '../dto/common.dto';
import { CreateCountryDTO, UpdateCountryDTO } from '../dto/countries.dto';
import { DeleteResult } from 'typeorm';

@Controller('countries')
export class CountriesController {
  @Inject(CountriesService)
  private readonly countriesService: CountriesService;


  @Get('/getAllCountries')
  @ApiResponse({
    status: 200,
    description: 'Get all countries from DB',
    type: [Countries],
  })
  async getAllCountries(@Res() response: Response<Countries[]>): Promise<void> {
    try {
      response.status(200).send(await this.countriesService.getAllCountries());
    } catch (error) {
      response.status(401).send(error.message);
      throw new InternalServerErrorException(error.message);
    }
  }

  @Get('/getCountriesByID')
  @ApiResponse({
    status: 200,
    description: 'Get a country by id',
    type: Countries,
  })
  async getCountriesByID(
    @Query('id') id: number,
    @Res() response: Response<Countries>,
  ): Promise<void> {
    try {
      response
        .status(200)
        .send(await this.countriesService.getCountriesByID(id));
    } catch (error) {
      response.status(401).send(error.message);
      throw new InternalServerErrorException(error.message);
    }
  }

  @Get('/getCountriesByName')
  @ApiResponse({
    status: 200,
    description: 'Get a country by name ',
    type: [Countries],
  })
  @ApiQuery({
    name: 'countryName',
    type: 'string',
    example: 'France',
    required: true,
  })
  async getCountriesByName(
    @Query('countryName') countryName: string,
    @Res() response: Response<Countries[]>,
  ): Promise<void> {
    try {
      response
        .status(200)
        .send(await this.countriesService.getCountriesByName(countryName));
    } catch (error) {
      response.status(401).send(error.message);
      throw new InternalServerErrorException(error.message);
    }
  }

  @Post('/createCountry')
  @ApiBody({
    type: CreateCountryDTO,
  })
  async createCountry(
    @Body() body: any,
    @Res() response: Response<CreateResultDTO>,
  ): Promise<void> {
    try {
      response
        .status(201)
        .send(await this.countriesService.createCountry(body));
    } catch (error) {
      response.status(401).send(error.message);
      throw new InternalServerErrorException(error.message);
    }
  }

  @Put('/updateCountry')
  @ApiBody({
    type: UpdateCountryDTO,
  })
  async updateCountry(
    @Body() body: any,
    @Res() response: Response<UpdateResultDTO>,
  ): Promise<void> {
    try {
      response
        .status(201)
        .send(await this.countriesService.updateCountry(body));
    } catch (error) {
      response.status(401).send(error.message);
      throw new InternalServerErrorException(error.message);
    }
  }

  @Delete('/deleteCountry')
  @ApiBody({
    type: DeleteDTO,
  })
  async deleteCountry(
    @Body() body: any,
    @Res() response: Response<DeleteResult>,
  ): Promise<void> {
    try {
      response
        .status(201)
        .send(await this.countriesService.deleteCountry(body));
    } catch (error) {
      response.status(401).send(error.message);
      throw new InternalServerErrorException(error.message);
    }
  }
}
