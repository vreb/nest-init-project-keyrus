## :building_construction: Stack

- :hotsprings: Node JS with the framework [Nest.js](https://nestjs.com/)

  - [TypeScript](https://www.typescriptlang.org/) - TypeScript is a **strongly typed programming language** that builds on JavaScript, giving you better tooling at any scale.
  - [TypeORM](https://typeorm.io/) - TypeORM can run in NodeJS, it's highly influenced by other ORMs, such as [Hibernate]. (http://hibernate.org/orm/). **TypeORM is known for high quality, scalable, maintainable applications the most productive way.**
  - [Swagger UI](https://swagger.io/) - Swagger UI is a collection of HTML, JavaScript, and CSS assets that dynamically generate beautiful documentation from a Swagger-compliant API. **Allows to visualize and interact with the API’s resources without having any of the implementation logic in place.**

- :whale: A [dockerized](https://www.docker.com/) database with :
  - :floppy_disk: a [PostGreSQL](https://www.postgresql.org/) database
  - :eyes: [PGAdmin](https://www.pgadmin.org/) for data visualization & editing

## :construction_worker: Prerequisites

In order to run this project, you will need :

- [Node.js](https://nodejs.org/) (version ≥ 12)
  - you can run`node -v` to check which version of Node you are currently on
  - you can also use [NVS](https://www.npmjs.com/package/nvs) to simplify Node version management
  - here's a full tutorial on Windows : [How to install Node.js & NPM](https://phoenixnap.com/kb/install-node-js-npm-on-windows)
- [Yarn](https://classic.yarnpkg.com/lang/en/docs/install/#windows-stable) : Yarn is a fast, reliable, secure dependency manager. Yarn is way faster than npm because it stores dependencies locally while npm fetches packages from online for each 'install' command.
  - You can install yarn with : `npm install --global yarn`

## :gear: Init

### Database setup

You need to set up your database (with Docker) :

```bash
 docker-compose up -d
```

You can then access to pgAdmin via the following URI. You'll find the login/password in the _docker-compose.yml_.

```bash
 localhost:5050
```

You can create a server according to the predefined parameters of the .env :

```bash
DATABASE_TYPE=postgres
DATABASE_HOST=localhost
DATABASE_NAME=tremplin_db
DATABASE_USER=postgres
DATABASE_PASSWORD=postgres
DATABASE_PORT=5432
```
![serverRegister.png](./images/serverRegister.png)

![serverRegister2.png](./images/serverRegister2.png)

_NB: as the database is dockerized, you can enter the container name in the Host name field._

You can then create a table inside the database :
_Servers/tremplin_db/Databases/tremplin_bd/Schemas/public/Tables_

For this example project, we've setup a single table named _countries_ :

```bash
CREATE TABLE public.countries
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY,
    name character varying,
    population integer,
    PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS public.countries
    OWNER to postgres;
```

This will generate a database with theses columns :
| id [PK] | name | population |  
|--|--|--|
|integer|character varying | integer |

### :small_airplane: Code Init

You need to generate node_module & dist folders into your local environment :

```bash
 yarn install
```

## :rocket: Run

You can start the project with the following command :

```bash
 yarn start:dev
```

_NB : the :dev is useful in order to restart the application at each change. You can also simply run with yarn start._
The application with swagger will be available at this URI :

```bash
 localhost:3003/api
```

## :eyeglasses: Play with swagger

Swagger can act like a simple [Postman](https://www.postman.com/). It is useful for CRUD requests :

![get.png](./images/get.png)

![post.png](./images/post.png)

![put.png](./images/put.png)

![delete.png](./images/delete.png)

## 🔗 Links

This tutorial was built thanks to the help of :

- https://docs.nestjs.com/first-steps#prerequisites
- https://docs.nestjs.com/recipes/crud-generator
- https://dba.stackexchange.com/questions/1281/how-do-i-specify-that-a-column-should-be-auto-incremented-in-pgadmin
