import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';

export class CreateCountryDTO {
  @ApiProperty({
    example: 'France',
    description: 'Country Name',
  })
  @IsString()
  public name?: string | null;

  @ApiProperty({
    example: '100',
    description: 'Population (in millions) of the country',
  })
  @IsNumber()
  public population?: number | null;
}

export class UpdateCountryDTO {
  @ApiProperty({
    example: '1',
    description: 'Id of country in DB',
  })
  @IsNumber()
  public id?: number | null;

  @ApiProperty({
    example: 'France',
    description: 'Country Name',
  })
  @IsString()
  public name?: string | null;

  @ApiProperty({
    example: '100',
    description: 'Population (in millions) of the country',
  })
  @IsNumber()
  public population?: number | null;
}
