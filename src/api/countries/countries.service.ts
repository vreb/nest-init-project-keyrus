import { Injectable } from '@nestjs/common';
import {
  DeleteResult,
  Equal,
  FindOptionsWhere,
  ILike,
  Repository,
} from 'typeorm';
import { Countries } from '../entity/countries.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateResultDTO, DeleteDTO, UpdateResultDTO } from '../dto/common.dto';
import { CreateCountryDTO, UpdateCountryDTO } from '../dto/countries.dto';

@Injectable()
export class CountriesService {
  @InjectRepository(Countries)
  private countriesRepository: Repository<Countries>;

  async getAllCountries(): Promise<Countries[]> {
    const response = await this.countriesRepository.find({
      order: { id: 'ASC' },
    });
    return response;
  }

  async getCountriesByID(id: number): Promise<Countries> {
    return this.countriesRepository.findOne({
      where: { id },
      order: { id: 'ASC' },
    });
  }

  async getCountriesByName(countryName: string): Promise<Countries[]> {
    const findOptionsWhere: FindOptionsWhere<Countries> = {};
    if (countryName !== '') {
      const firstNameILike: string = '%' + countryName + '%';
      findOptionsWhere.name = ILike(firstNameILike);
    }
    return this.countriesRepository.find({
      where: findOptionsWhere,
    });
  }

  async createCountry(body: CreateCountryDTO): Promise<CreateResultDTO> {
    const bodySaveCountry = {
      name: body.name,
      population: body.population,
    };

    const data = await this.countriesRepository.save(bodySaveCountry);

    const success = true;
    const message = `Country with name ${body.name} has been created`;
    const bodyReturn = {
      success,
      data,
      message,
    };
    return bodyReturn;
  }

  async updateCountry(body: UpdateCountryDTO): Promise<UpdateResultDTO> {
    const findCondition: FindOptionsWhere<Countries> = {
      id: Equal(body.id),
    };

    const updateResult = await this.countriesRepository.update(findCondition, {
      name: body.name,
      population: body.population,
    });
    const message = `Entity countries with id ${body.id} updated`;
    const updateBody = {
      updateResult,
      message,
    };
    return updateBody;
  }

  async deleteCountry(body: DeleteDTO): Promise<DeleteResult> {
    const findCondition: FindOptionsWhere<Countries> = {
      id: Equal(body.id),
    };
    return await this.countriesRepository.delete(findCondition);
  }
}
